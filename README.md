# Project Test Ridho


1. Pembuatan base template menggunakan laravel
2. Pembuatan migration class untuk tabel MS_PRODUCT berisi ID, NamaProduk, Qty
3. Pembuatan modul login (migration class dan halaman). Credential menggunakan username dan password
4. Pembuatan halaman dashboard berisi statistik jumlah user dan jumlah produk terdaftar
5. Pembuatan RESTFul API yaitu endpoint untuk inquiry produk, menggunakan credential Oauth2.0/JWT
